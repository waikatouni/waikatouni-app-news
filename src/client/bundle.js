import {registerTileComponent} from "@ombiel/cm-tile-sdk";
import NewsFeedTile from "./tiles/news-feed-tile/components/news-feed-tile";

registerTileComponent(NewsFeedTile,"NewsFeedTile");
