import React from "react";
import moment from "moment";

import {
  HBlock,
  Tile,
  TileBoard,
  VBlock,
  TileFace,
  // TextGroupItem,
  // TextGroup,
  // VBlock,
  TextBox,
  // TextLine,
  ImageBox,
  // Badge,
  // Layer,
  // SvgBox,
  
  // useTileProps,
  // useCallback,
  // useCycle,
  // useFetch,
  // useMemo,
  // useReducer,
  // useRef,
  useServerAction,
  // useStash,
  // useState,
  // useTicker,
  // useTileConfig,
  // useTicker,
  useTimedCycle,
  // useTimer,

} from "@ombiel/cm-tile-sdk";

export default function NewsFeedTile() {
  var title = "Fetching University News...";
  var body = "Tip: Tap on news articles to find out more";

  var imageURL = "https://img.icons8.com/color/200/000000/worldwide-delivery.png";
  var clickURL = "https://www.waikato.ac.nz/news-events";

  var date = moment(new Date()).format("LL");

  const [currentItem] = useTimedCycle([0,1,2,3,4,5,6,7,8,9],15);

  const [{ responseBody, error }] = useServerAction("news", {
    method: "GET",
    isJsonResponse: false,
    updateRate: 2000,
  });

  if (responseBody) {
    var parser = new DOMParser();
    var xmlData = responseBody;

    var parsedData = parser.parseFromString(xmlData, 'text/xml');
    var channelData = parsedData.getElementsByTagName("channel")[0];
    var newdata = channelData && channelData.getElementsByTagName("item")[currentItem];

    title = newdata && newdata.getElementsByTagName("title")[0].textContent;
    body = newdata && newdata.getElementsByTagName("description")[0].textContent;
    imageURL = newdata && newdata.getElementsByTagName("enclosure")[0] ? newdata.getElementsByTagName("enclosure")[0].getAttribute("url") : "https://img.icons8.com/doodle/240/000000/news.png";
    clickURL = newdata && newdata.getElementsByTagName("link")[0].textContent;
    date = newdata && moment(newdata.getElementsByTagName("pubDate")[0].textContent).format("LL");
  } else if (error) {
    title = "Sorry, something went wrong";
    body = "Please report this issue to campusm@waikato.ac.nz";
  }

  const divider = [0, false];
  const boxProps = {
    backgroundColor: '#ffffff',
    backgroundOpacity: 0.3,
  };

  return (
    <Tile>
      <TileBoard>
        <TileFace key={currentItem} borderRadius={0} onClick={clickURL}>
          <HBlock key={currentItem} divider={divider}>
            <ImageBox backgroundColor="#F2F2F2" src={imageURL} imageFit={imageURL === ("https://img.icons8.com/color/200/000000/worldwide-delivery.png" || "https://img.icons8.com/doodle/240/000000/news.png") ? "contain" : "cover"} />
            <VBlock key={currentItem} flex={2} backgroundColor="#F2F2F2" divider={divider} {...boxProps}>
              <TextBox backgroundColor="#F2F2F2" color="#BE0403" minFontSize={15} padding={[5, 5]} textAlign="left" bold>{title}</TextBox>
              <TextBox backgroundColor="#F2F2F2" color="#353535" minFontSize={12} padding={[0, 5]} textAlign="left" italic>{body}</TextBox>
              <TextBox backgroundColor="#F2F2F2" color="#353535" maxFontSize={12} padding={[5, 5, 10, 5]} textAlign="left" maxSize={16} bold>{date}</TextBox>
            </VBlock>
          </HBlock>
        </TileFace>
      </TileBoard>
    </Tile>
  );
}
