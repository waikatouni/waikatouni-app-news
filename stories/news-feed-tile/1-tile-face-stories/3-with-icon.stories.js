// eslint-disable-next-line no-unused-vars
import React from 'react';
import { storiesOf } from '@storybook/react';

// import { withKnobs } from '@storybook/addon-knobs';
import { addTileFaceStories, generateImage } from "@ombiel/cm-tile-sdk/dev";

import NewsFeedTileFace from '../../../src/client/tiles/news-feed-tile/components/news-feed-tile-face';

const image = generateImage("My Tile    ","#eeeeee");

const stories = storiesOf('News Feed | Tile Face / With Icon', module);

addTileFaceStories(stories,<NewsFeedTileFace text="News Feed" icon={image} />);
