// eslint-disable-next-line no-unused-vars
import React from 'react';
import { storiesOf } from '@storybook/react';

// import { withKnobs } from '@storybook/addon-knobs';
import { addTileFaceStories } from "@ombiel/cm-tile-sdk/dev";

import NewsFeedTileFace from '../../../src/client/tiles/news-feed-tile/components/news-feed-tile-face';


const stories = storiesOf('News Feed | Tile Face / Just Text', module);

addTileFaceStories(stories,<NewsFeedTileFace text="News Feed" />);
